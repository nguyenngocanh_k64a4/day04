<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>    
    <style type="text/css">
        *:focus {
            outline: none;
        }

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 70vh;
        }

        .validation {
            color:#FF3333;
        }

        .wrapper {
            width: 25%;
            padding: 40px;
            border: 1.5px solid dodgerblue;


        }

        .field {
            margin-bottom: 25px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .field__label {
            color: #eeeeee;
            background-color: #3a94e5;
            padding: 12px 10px;
            width: 30%;
        }
        .field__input {
            font-size: 14px;
            font-weight: 400;
            padding: 12px;
            border-radius: 0;
            border: 1px solid #3a94e5;
            width: 55%;
        }

        .gender {
            display: flex;
            width: 55%;
            gap: 20px;
        }

        select {
            width: 55%;
            height: 43px;
            border: 1px solid #3a94e5;
        }

        .button {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #5fa14a;
            padding: 12px 32px;
            margin-top: 15px;
            border-radius: 10px;
        }

    </style>
</head>
<body>

    <?php
        $sex = array("Nam", "Nữ");
        $faculty = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
    ?>

    <div class="main">
        <div class="wrapper">

            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if(empty($_POST["fullname"])) {
                    echo '<span class="validation">Hãy nhập Họ tên</span> <br>';
                }

                if(empty($_POST["sex"])) {
                    echo '<span class="validation">Hãy chọn Giới tính</span> <br>';
                }

                if(empty($_POST["faculties"])) {
                    echo '<span class="validation">Hãy chọn Phân khoa</span> <br>';
                }

                if(empty($_POST["bithday"])) {
                    echo '<span class="validation">Hãy nhập Ngày sinh</span> <br>';
                } else {
                    $date = explode("/",$_POST['bithday']);
                    if(!checkdate($date[1] ,$date[0] ,$date[2]))
                    {
                        echo '<span class="validation">Hãy nhập Ngày sinh đúng định dạng</span> <br>';
                    }
                }
            }
            ?>
            <br>

            <form action="" method="post">
                <div class="field">
                    <label for="fullname" class="field__label">Họ và tên *</label>
                    <input type="text" name="fullname" class="field__input"/>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính *</label>
                    <div class="gender">
                        <?php
                            for($i = 0; $i < count($sex); $i++) {
                                echo '<div>';
                                echo '<input type="radio" id="' . $sex[$i] . '" name="sex" value="' . $sex[$i] . '">';
                                echo '<label style="margin-left: 12px; user-select: none; cursor: pointer" for="' . $sex[$i] . '">' . $sex[$i] . '</label><br>';
                                echo '</div>';

                            }
                        ?>
                    </div>
                    
                </div>

                <div class="field">
                    <label for="faculties" class="field__label">Phân khoa *</label>
                    <select name="faculties">
                        <option value="0" selected></option>
                        <?php
                            foreach($faculty as $x => $x_value) {
                                echo '<option value="' . $x . '">' . $x_value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <div class="field">
                    <label for="bithday" class="field__label">Ngày sinh *</label>
                    <input name="bithday" class="date form-control field__input" type="text" style="padding: 22px 12px;">
                </div>

                <div class="field">
                    <label for="address" class="field__label">Địa chỉ</label>
                    <input type="text" name="address" class="field__input"/>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">  
    $('.date').datepicker({  
       format: 'dd/mm/yyyy'  
     });  
</script>  
</body>

</html>